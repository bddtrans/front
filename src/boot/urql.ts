import urql, { cacheExchange, fetchExchange } from '@urql/vue'
import { App } from 'vue'

export default function boot ({ app }: {app: App}) {
  app.use(urql, {
    url: import.meta.env.VITE_API_URL || 'http://localhost:8000/graphql',
    exchanges: [cacheExchange, fetchExchange]
  })
}
