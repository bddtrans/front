export interface Doctor {
  kind?: string
  description?: string
  firstName?: string
  lastName?: string
  address?: string
  postalCode?: string
  city?: string
  country?: string
  infoLink?: string
  pk?: string
}

export interface DoctorQuery{
  kind?: string
  firstName?: string
  lastName?: string
  postalCode?: string
  city?: string
}
